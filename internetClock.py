import datetime
import os
import time
from urllib.request import urlopen

class Clock:
	def __init__(self):
		self.offset = 0
		self.getTimeZoneData()
		self.time = self.getOnlineUTCTime()
		self.timeLastUpdate = self.time
		self.LEDNumberDict = { "1" : [[0, 1, 1, 0], [1, 1, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0], [1, 1, 1, 1]],\
								"2" : [[0, 1, 1, 0], [1, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0 ,0], [1, 1, 1, 1]],\
								"3" : [[1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 1, 0], [1, 0, 0, 1], [0, 1, 1, 0]],\
								"4" : [[0, 0, 1, 0], [0, 1, 1, 0], [1, 0, 1, 0], [1, 1, 1, 1], [0, 0, 1, 0]],\
								"5" : [[1, 1, 1, 1], [1, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 1], [1, 1, 1, 1]],\
								"6" : [[0, 1, 1, 0], [1, 0, 0, 0], [1, 1, 1, 0], [1, 0, 0, 1], [0, 1, 1, 0]],\
								"7" : [[1, 1, 1, 1], [0, 0 ,0 ,1], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]],\
								"8" : [[0, 1, 1, 0], [1, 0, 0, 1], [0, 1, 1, 0], [1, 0, 0, 1], [0, 1, 1, 0]],\
								"9" : [[0, 1, 1, 0], [1, 0, 0, 1], [0, 1, 1, 1], [0, 0, 0, 1], [0, 1, 1, 0]],\
								"0" : [[0, 1, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 1, 0]],\
								":" : [[0, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0]],\
								"-" : [[0, 0, 0, 0], [0, 0, 0, 0], [0, 1, 1, 0], [0, 0, 0, 0], [0, 0, 0, 0]]}

	def getTimeZoneData(self):
		if (time.localtime().tm_isdst == 0):
			self.offset = int(time.timezone)
		else:
			self.offset = int(time.altzone)
		self.offset //= -3600
	
	def getOnlineUTCTime(self):
		try:
			webpage = urlopen("http://just-the-time.appspot.com/")
			internetData = str(webpage.read())[2:-2]
			internetDate = internetData.split()[0]
			internetTime = internetData.split()[1]
			internetTime = str(int(internetTime[:2]) + self.offset) + internetTime[2:]
			return datetime.datetime(int(internetDate[0:4]), int(internetDate[5:7]), int(internetDate[8:10]), int(internetTime[:2]), int(internetTime[3:5]), int(internetTime[6:7]))
		except:
			return datetime.datetime.now()

	def updateTime(self):
		if (datetime.datetime.now()).timestamp() - (self.timeLastUpdate).timestamp() > (self.offset + 1)*3600000: # Check internet once per hour
			self.time = self.getOnlineUTCTime()
			self.timeLastUpdate = self.time
		else:
			timeElapsed = int((datetime.datetime.now()).timestamp() - (self.timeLastUpdate).timestamp())
			self.time = self.timeLastUpdate + datetime.timedelta(seconds = timeElapsed)

	def display(self):
		timeLastCall = 0
		while True:
			timeNow = (datetime.datetime.now()).timestamp()
			if timeNow -  timeLastCall > 0.5: # Don't update the screen more often than once per half a second
				timeStrArr = ["" for x in range(len(self.LEDNumberDict["0"]))]
				for element in (self.time).strftime("%X"):
					for idx, part in enumerate(self.LEDNumberDict[element]):
						for switch in part:
							if bool(switch):
								LED = "0"
							else:
								LED = " "
							timeStrArr[idx] += LED
						timeStrArr[idx] += " "
				timeStr = ""
				for row in timeStrArr:
					timeStr += row + '\n'
				os.system("clear")
				print(timeStr)
				timeLastCall = (datetime.datetime.now()).timestamp()
				self.updateTime()

LEDClock = Clock()
LEDClock.display()
