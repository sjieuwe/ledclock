import datetime
from LEDNumber import LEDNumber

class Clock:
	# Redo construcor so LED indices aren't hardcoded anymore
	def __init__(self):
		self.tenHours = LEDNumber([0, 3, 6, 9, 12, 15, 18])
		self.hours = LEDNumber([21, 24, 27, 30, 33, 36, 39])
		self.tenMins = LEDNumber([42, 45, 48, 51, 54, 57, 60])
		self.minutes = LEDNumber([63, 66, 69, 72, 75, 78, 81])

	def obtainLEDInfo(self, time = datetime.datetime.now()):
		timeString = time.strftime("%X")
		self.activeLEDs = []
		self.inactiveLEDs = []
		self.activeLEDs.extend(self.tenHours.getActiveLEDs(int(timeString[0])))
		self.activeLEDs.extend(self.hours.getActiveLEDs(int(timeString[1])))
		self.activeLEDs.extend(self.tenMins.getActiveLEDs(int(timeString[3])))
		self.activeLEDs.extend(self.minutes.getActiveLEDs(int(timeString[4])))
		self.inactiveLEDs.extend(\
			self.tenHours.getInactiveLEDs(int(timeString[0])))
		self.inactiveLEDs.extend(\
			self.hours.getInactiveLEDs(int(timeString[1])))
		self.inactiveLEDs.extend(\
			self.tenMins.getInactiveLEDs(int(timeString[3])))
		self.inactiveLEDs.extend(\
			self.minutes.getInactiveLEDs(int(timeString[4])))