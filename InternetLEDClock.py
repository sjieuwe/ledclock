from Clock import Clock
import datetime
from neopixel import *
import time
import threading
from urllib.request import urlopen

# LED strip configuration:

LED_COUNT      = 120      # Number of LED pixels.
LED_PIN        = 12      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal
LED_CHANNEL    = 0


class InternetLEDClock:
	def __init__(self):
		self.getTimeZoneData()
		self.LEDClock = Clock() # good spot to define which LEDs are used
		self.LEDRefreshThread = threading.Thread(target=self.updateClockFace)
		self.timeUpdateLock = threading.Lock()
		self.offset = 0
		self.strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
		self.strip.begin()
		self.time = self.getOnlineUTCTime()
		self.timeLastUpdate = self.time
		self.timeThread = threading.Thread(target=self.updateTime)
		self.timeThread.start()
		

	def getTimeZoneData(self):
		if (time.localtime().tm_isdst == 0):
			self.offset = int(time.timezone)
		else:
			self.offset = int(time.altzone)
		self.offset //= -3600
	
	def getOnlineUTCTime(self):
		try:
			webpage = urlopen("http://just-the-time.appspot.com/")
			internetData = str(webpage.read())[2:-2]
			internetDate = internetData.split()[0]
			internetTime = internetData.split()[1]
			internetTime = str(int(internetTime[:2]) + self.offset) + internetTime[2:]
			return datetime.datetime(int(internetDate[0:4]), int(internetDate[5:7]), int(internetDate[8:10]), int(internetTime[:2]), int(internetTime[3:5]), int(internetTime[6:7]))
		except:
			return datetime.datetime.now()

	def updateTime(self):
		while True:
			print(self.time)
			if (datetime.datetime.now()).timestamp() - (self.timeLastUpdate).timestamp() > (self.offset + 1)*3600000: # Check internet once per hour
				self.timeUpdateLock.acquire()
				self.time = self.getOnlineUTCTime()
				self.timeLastUpdate = self.time
			else:
				timeElapsed = int((datetime.datetime.now()).timestamp() - (self.timeLastUpdate).timestamp())
				self.timeUpdateLock.acquire()
				self.time = self.timeLastUpdate + datetime.timedelta(seconds = timeElapsed)
			self.timeUpdateLock.release()
			time.sleep(0.5)

	def refreshLEDs(self):
		self.LEDClock.obtainLEDInfo(self.time)

	def getActiveLEDs(self):
		return self.LEDClock.activeLEDs

	def getInactiveLEDs(self):
		return self.LEDClock.inactiveLEDs

	def updateClockFace(self):
		try:
			while True:
				if self.timeUpdateLock.locked() == False:
					self.refreshLEDs()
					print(len(self.getActiveLEDs()))
					for LED in self.getActiveLEDs():
						self.strip.setPixelColor(LED, Color(0, 255, 0))
					for LED in self.getInactiveLEDs():
						self.strip.setPixelColor(LED, Color(0, 0, 0))
				self.strip.show()
				time.sleep(0.5) # Don't update more than once per half a second
		except:
		    for LED in range(self.strip.numPixels()):
        		self.strip.setPixelColor(LED, Color(0, 0, 0))

internetLEDClock = InternetLEDClock()
internetLEDClock.LEDRefreshThread.start()
