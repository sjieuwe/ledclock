import time
from neopixel import *

LED_COUNT      = 120      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 128     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

def purple(strip):
	for LED in range(strip.numPixels()/2):
		strip.setPixelColor(LED, Color(0, 255, 0))
		strip.setPixelColor(strip.numPixels() - LED, Color(0, 0, 255))
		strip.show()
		time.sleep(250.0/1000) # in milliseconds
	for LED in range(strip.numPixels()/2, strip.numPixels()):
		strip.setPixelColor(LED, Color(0, 64, 64))
		strip.setPixelColor(strip.numPixels() - LED, Color(0, 64, 64))
		strip.show()
		time.sleep(250.0/1000) # in milliseconds


def setRGB(strip):
	for LED in range(strip.numPixels()):
		if LED % 3 == 0:
			strip.setPixelColor(LED, Color(255, 0, 0))
		if LED % 3 == 1:
			strip.setPixelColor(LED, Color(0, 255, 0))
		if LED % 3 == 2:
			strip.setPixelColor(LED, Color(0, 0, 255))
		strip.show()
		time.sleep(25.0/1000) # in milliseconds

def turnOff(strip):
	for LED in range(strip.numPixels()):
		strip.setPixelColor(LED, Color(0, 0, 0))
		strip.show()
		time.sleep(LED/1000.0) # in milliseconds

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()
purple(strip)
turnOff(strip)