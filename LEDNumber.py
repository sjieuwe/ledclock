class LEDNumber:
	def __init__(self, initialLEDs, nrOfLEDsPerPt = 3):
		self.botLeft = [initialLEDs[0] + LED for LED in range(nrOfLEDsPerPt)]
		self.bot = [initialLEDs[1] + LED for LED in range(nrOfLEDsPerPt)]
		self.botRight = [initialLEDs[2] + LED for LED in range(nrOfLEDsPerPt)]
		self.middle = [initialLEDs[3] + LED for LED in range(nrOfLEDsPerPt)]
		self.topLeft = [initialLEDs[4] + LED for LED in range(nrOfLEDsPerPt)]
		self.top = [initialLEDs[5] + LED for LED in range(nrOfLEDsPerPt)]
		self.topRight = [initialLEDs[6] + LED for LED in range(nrOfLEDsPerPt)]

	def getActiveLEDs(self, number):
		activeLEDs = []
		if number == 0:
			activeLEDs.extend(self.botLeft)
			activeLEDs.extend(self.bot)
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.topLeft)
			activeLEDs.extend(self.top)
			activeLEDs.extend(self.topRight)
		if number == 1:
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.topRight)
		if number == 2:
			activeLEDs.extend(self.botLeft)
			activeLEDs.extend(self.bot)
			activeLEDs.extend(self.middle)
			activeLEDs.extend(self.top)
			activeLEDs.extend(self.topRight)
		if number == 3:
			activeLEDs.extend(self.bot)
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.middle)
			activeLEDs.extend(self.top)
			activeLEDs.extend(self.topRight)
		if number == 4:
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.middle)
			activeLEDs.extend(self.topLeft)
			activeLEDs.extend(self.topRight)
		if number == 5:
			activeLEDs.extend(self.bot)
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.middle)
			activeLEDs.extend(self.topLeft)
			activeLEDs.extend(self.top)
		if number == 6:
			activeLEDs.extend(self.botLeft)
			activeLEDs.extend(self.bot)
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.middle)
			activeLEDs.extend(self.topLeft)
			activeLEDs.extend(self.top)
		if number == 7:
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.top)
			activeLEDs.extend(self.topRight)
		if number == 8:
			activeLEDs.extend(self.botLeft)
			activeLEDs.extend(self.bot)
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.middle)
			activeLEDs.extend(self.topLeft)
			activeLEDs.extend(self.top)
			activeLEDs.extend(self.topRight)
		if number == 9:
			activeLEDs.extend(self.bot)
			activeLEDs.extend(self.botRight)
			activeLEDs.extend(self.middle)
			activeLEDs.extend(self.topLeft)
			activeLEDs.extend(self.top)
			activeLEDs.extend(self.topRight)
		return activeLEDs

	def getInactiveLEDs(self, number):
		inactiveLEDs = []
		if number == 0:
			inactiveLEDs.extend(self.middle)
		if number == 1:
			inactiveLEDs.extend(self.botLeft)
			inactiveLEDs.extend(self.bot)
			inactiveLEDs.extend(self.middle)
			inactiveLEDs.extend(self.topLeft)
			inactiveLEDs.extend(self.top)
		if number == 2:
			inactiveLEDs.extend(self.botRight)
			inactiveLEDs.extend(self.topLeft)
		if number == 3:
			inactiveLEDs.extend(self.botLeft)
			inactiveLEDs.extend(self.topLeft)
		if number == 4:
			inactiveLEDs.extend(self.botLeft)
			inactiveLEDs.extend(self.bot)
			inactiveLEDs.extend(self.top)
		if number == 5:
			inactiveLEDs.extend(self.botLeft)
			inactiveLEDs.extend(self.topRight)
		if number == 6:
			inactiveLEDs.extend(self.topRight)
		if number == 7:
			inactiveLEDs.extend(self.botLeft)
			inactiveLEDs.extend(self.bot)
			inactiveLEDs.extend(self.middle)
			inactiveLEDs.extend(self.topLeft)
		if number == 9:
			inactiveLEDs.extend(self.botLeft)
		return inactiveLEDs